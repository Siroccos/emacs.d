;; -*- coding: utf-8; lexical-binding: t -*-

;; Increase startup time
(setq gc-cons-threshold 100000000)

(setq inhibit-startup-message t)

;; utf-8
(prefer-coding-system 'utf-8-unix)
(set-default-coding-systems 'utf-8)

;; Clean interface
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)

(defalias 'yes-or-no-p 'y-or-n-p)

(electric-pair-mode t)

;;(set-frame-font "Fira Code 10" nil t)
(set-frame-font "JetBrainsMono Nerd Font 10" nil t)

;; No bell
(setq visible-bell nil)

;; Custom calendar
(setq calendar-week-start-day 1) ; Start monday

(global-set-key (kbd "M-s i") 'imenu)

;; Define custom directory (conf, elpa, ...)
(setq jgi/cache-dir (expand-file-name "cache/" user-emacs-directory))
(setq package-user-dir (expand-file-name "elpa/" jgi/cache-dir)
      recentf-save-file (expand-file-name "recentf" jgi/cache-dir)
      auto-save-file-name-transforms `((".*" ,(file-name-as-directory (expand-file-name "auto-saves/" jgi/cache-dir)) t))
      backup-directory-alist `(("." . "~/.emacs.d/cache/backup/"))
      backup-by-copying t
      transient-history-file (expand-file-name "transient/history.el" jgi/cache-dir)
      transient-levels-file (expand-file-name "transient/levels.el" jgi/cache-dir)
      transient-values-file (expand-file-name "transient/values.el" jgi/cache-dir)
      auto-save-list-file-prefix (expand-file-name "recovery/saves-" jgi/cache-dir)
      bookmark-default-file (expand-file-name "bookmarks" jgi/cache-dir))

;; Manage package initialisation
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;;; Theme and visuals
;; M-x all-the-icons-install-fonts 
(use-package all-the-icons)

(use-package doom-themes
  :init (load-theme 'doom-one t)
  :config
  (setq doom-themes-enable-italic t
	doom-themes-enable-bold t))

(use-package spacemacs-theme
  :disabled
  :defer t
  :init (load-theme 'spacemacs-dark t))

(set-face-attribute 'font-lock-comment-face nil :italic t)

(use-package doom-modeline
  :init (doom-modeline-mode 1))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
		term-mode-hook
		shell-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;;; Shortcut help
(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

;;; Ivy
(use-package ivy
  :demand
  :diminish
  :bind (("C-S-s" . swiper-isearch))
  :config
  (setq ivy-use-virtual-buffers t
	ivy-extra-directories nil
	ivy-count-format "(%d/%d) ")
  (ivy-mode 1))

(use-package counsel
  :demand
  :config
  (counsel-mode 1))

;;; Projectile
(use-package projectile
  :diminish projectile-mode
  :config
  (setq projectile-completion-system 'ivy
	projectile-cache-file (expand-file-name "projectile.cache" jgi/cache-dir)
	projectile-known-projects-file (expand-file-name "projectile-known-projects.eld" jgi/cache-dir))
  (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map))

;;; Company
(use-package company
  :demand
  :bind ("M-/" . company-complete-common)
  :init
  (setq company-require-match 'never
	company-idle-delay 0.1
	company-minimum-prefix-length 2)
  :config
  (add-hook 'after-init-hook 'global-company-mode))

(use-package company-box
  :hook (company-mode . company-box-mode)
  :config
  (setq company-box-doc-enable nil
	company-box-scrollbar nil))

;;; Sort ivy/company by frequencies
(use-package prescient)

(use-package ivy-prescient)

(use-package company-prescient)

;;; Snippets
(use-package yasnippet
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :after yasnippet)

(defun company-mode/backend-with-yas (backend)
 (append (if (consp backend) backend (list backend))
	  '(:with company-yasnippet)))
(setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))

;; Template new file
(use-package autoinsert
  :disabled
  :ensure nil
  :init
  ;; Don't want to be prompted before insertion
  (setq auto-insert-query nil)

  (setq auto-insert-directory (expand-file-name "templates" jgi/cache-dir))
  (add-hook 'find-file-hook 'auto-insert)
  (auto-insert-mode 1)

  :config
					;  (define-auto-insert "\\.html?$" "default-html.html"))
  )

(delete-selection-mode 1)

;;; Better navigation and edition
(use-package avy
  :bind ("M-s M-s" . avy-goto-char-2))

(use-package iedit
  :bind ("C-;" . iedit-mode))

(use-package visual-regexp
  :bind ("M-%" . vr/query-replace))

(use-package expand-region
  :bind ("C-=" . er/expand-region))

;; Magit
(use-package magit)

(use-package magit-todos)

(use-package diff-hl
  :config
  (global-diff-hl-mode)
  (diff-hl-flydiff-mode 1))

(fringe-mode 3)

;;; Org
(use-package org)
(add-hook 'org-mode-hook (lambda ()
			   (setq-local electric-pair-inhibit-predicate
				       `(lambda (c)
					  (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))
			   (set-face-attribute 'org-block-begin-line nil :italic t)
			   (set-face-attribute 'org-block-end-line nil :italic t)))
;(add-hook 'org-mode-hook 'visual-line-mode)
(add-hook 'org-mode-hook 'variable-pitch-mode)
(setq org-startup-indented t
      org-hide-emphasis-markers t
      org-startup-with-inline-images t
      org-image-actual-width '(300)
      org-agenda-files '("~/Documents/Notes/org/")
      org-link-abbrev-alist '(("note" . "~/Documents/Notes/org/%s"))
      org-html-html5-fancy t)
(use-package org-appear
  :hook (org-mode . org-appear-mode))

;; Open link in windows
;; TODO: Il faut remplacer le protocol file et non l'ajouter de nouveau
(add-to-list 'org-link-frame-setup '(file . find-file)) 

;; Add custom backend for #+ and custom note
(defconst jgi/note-directory "~/Documents/Notes/org/")

(defun org-keyword-backend (command &optional arg &rest ignored)
  (interactive (list 'interactive))
  (cl-case command
    (prefix (and (eq major-mode 'org-mode)
		 (cons (company-grab-line "^#\\+\\(\\w*\\)" 1)
		       t)))
    (candidates (mapcar #'downcase
			(cl-remove-if-not
			 (lambda (c) (string-prefix-p arg c))
			 (pcomplete-completions))))
    (ignore-case t)
    (duplicates t)))
(add-to-list 'company-backends 'org-keyword-backend)

(defun org-note-backend (command &optional arg &rest ignored)
  (interactive (list 'interactive))
  (cl-case command
    (prefix (and (eq major-mode 'org-mode)
		 (cons (company-grab-line ".*\\[\\[note:\\(\\w*\\)" 1)
		       t)))
    (candidates (delete "." (delete ".." (directory-files jgi/note-directory))))))
(add-to-list 'company-backends 'org-note-backend)


;; (let* ((variable-tuple
;;           (cond ((x-list-fonts "ETBembo")         '(:font "ETBembo"))
;;                 ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
;;                 ((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
;;                 ((x-list-fonts "Verdana")         '(:font "Verdana"))
;;                 ((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
;;                 (nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
;;          (base-font-color     (face-foreground 'default nil 'default))
;;          (headline           `(:inherit default :weight bold :foreground ,base-font-color)))

;;     (custom-theme-set-faces
;;      'user
;;      `(org-level-8 ((t (,@headline ,@variable-tuple))))
;;      `(org-level-7 ((t (,@headline ,@variable-tuple))))
;;      `(org-level-6 ((t (,@headline ,@variable-tuple))))
;;      `(org-level-5 ((t (,@headline ,@variable-tuple))))
;;      `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
;;      `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
;;      `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
;;      `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
;;      `(org-document-title ((t (,@headline ,@variable-tuple :height 2.0 :underline nil))))))

(custom-theme-set-faces
 'user
 '(variable-pitch ((t (:family "Iosevka Aile" :height 110))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-checkbox ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))

(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (emacs-lisp	. t)
   (org		. t)
   (shell	. t)
   (C		. t)
   (python	. t)
   (gnuplot	. t)
   (js		. t)
   (dot		. t)))
(setq org-src-fontify-natively t
      org-src-tab-acts-natively t
      org-confirm-babel-evaluate nil)

(use-package olivetti
  :config (setq-default olivetti-body-width 120))

(add-hook 'org-mode-hook
	  (lambda ()
	    (olivetti-mode 1)))

;;; Python
;; (use-package lsp-mode
;;   :init
;;   (setq lsp-keymap-prefix "C-c l")
;;   :hook ((python-mode . lsp)
;; 	 (lsp-mode . lsp-enable-which-key-integration))
;;   :commands lsp)
;; (use-package lsp-ui :commands lsp-ui-mode)
;; (use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
;; (use-package lsp-treemeacs :commands lsp-treemacs-errors-list :disabled)

(use-package eglot
  :hook
  (python-mode . eglot-ensure))

(put 'dired-find-alternate-file 'disabled nil)

(use-package tree-sitter)
(use-package tree-sitter-langs)

(use-package perspective
  :bind (("C-c k" . persp-kill-buffer*)
	 ("C-x b" . persp-ivy-switch-buffer))
  :init
  (persp-mode))

(use-package persp-projectile)

(use-package switch-window
  :bind (("C-x o" . switch-window)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(delete-selection-mode nil)
 '(package-selected-packages
   '(switch-window spacemacs-theme perspective olivetti yasnippet-snippets which-key visual-regexp use-package twilight-bright-theme tree-sitter-langs tao-theme rainbow-delimiters projectile org-appear magit-todos lsp-ui lsp-ivy ivy-prescient iedit expand-region eglot doom-themes doom-modeline diff-hl counsel company-prescient company-box avy))
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-checkbox ((t (:inherit (shadow fixed-pitch)))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
 '(variable-pitch ((t (:family "Iosevka Aile" :height 110)))))
